package com.hcl.PetsAppController.service;

public class RecordNotFoundException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	RecordNotFoundException(String message){
		super(message);
	}
}
