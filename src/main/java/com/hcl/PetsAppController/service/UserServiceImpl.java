package com.hcl.PetsAppController.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.PetsAppController.model.Pet;
import com.hcl.PetsAppController.model.User;
import com.hcl.PetsAppController.repository.UserRepository;
@Service
public class UserServiceImpl implements UserService{
	@Autowired
	private UserRepository userRepository;
	@Override
	public User addUser(User user) {
		return userRepository.save(user);
	}

	@Override
	public User updateUser(User user) {
		return userRepository.save(user);
	}

	@Override
	public List<User> listUsers() {
List<User> userList = (List<User>) userRepository.findAll();
        
        if(userList.size() > 0) {
            return userList;
        } else {
            return new ArrayList<User>();
        }
	}

	@Override
	public User getUserById(int userid) {
		
			
			Optional<User> option = userRepository.findById(userid);
			User user = null;
			if (option.isPresent()) {
				user = option.get();
			}
			return user;
		}

	

	@Override
	public void removeUser(int userid) {
		Optional<User> employee = userRepository.findById(userid);
        
        if(employee.isPresent()) 
        {
        	userRepository.deleteById(userid);
        }
        else{
        	throw new RecordNotFoundException("No employee record exist for given id");
        }
	}

	/*@Override
	public List<Pet> getAllMyPets(int userid) {
		
		return userRepository.getAllMyPets(userid);
	}*/
	

}
