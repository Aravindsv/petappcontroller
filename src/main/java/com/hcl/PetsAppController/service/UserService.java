package com.hcl.PetsAppController.service;

import java.util.List;

import com.hcl.PetsAppController.model.Pet;
import com.hcl.PetsAppController.model.User;

public interface UserService {
	 User addUser(User user);
	 User updateUser(User user);
	 List<User>listUsers();
	 User getUserById(int userid);
	 void removeUser(int userid);
	// List<Pet> getAllMyPets(int userid);
}
