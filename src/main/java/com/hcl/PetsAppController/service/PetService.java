package com.hcl.PetsAppController.service;

import java.util.List;

import com.hcl.PetsAppController.model.Pet;
import com.hcl.PetsAppController.model.User;


public interface PetService {
	Pet getPetById(int petid);
	 Pet savePet(Pet pet);
	 List<Pet>fetchAll();
 
}
