package com.hcl.PetsAppController.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.PetsAppController.model.Pet;
import com.hcl.PetsAppController.model.User;
import com.hcl.PetsAppController.repository.PetRepository;
@Service
public class PetServiceImpl implements PetService{
@Autowired
private PetRepository petRepository;
	@Override
	public Pet getPetById(int petid)throws RecordNotFoundException {
		
		Optional<Pet> option = petRepository.findById(petid);
		Pet pet = null;
		if (option.isPresent()) {
			pet = option.get();
		}
		 else{
	        	throw new RecordNotFoundException("No pet record exist for given id");
	        }
		return pet;
	}

	@Override
	public Pet savePet(Pet pet) {
		
		return petRepository.save(pet);
	}

	@Override
	public List<Pet> fetchAll() {
		
List<Pet> petList = (List<Pet>) petRepository.findAll();
        
        if(petList.size() > 0) {
            return petList;
        } else {
            return new ArrayList<Pet>();
        }
	}

}
