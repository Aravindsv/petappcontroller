package com.hcl.PetsAppController.repository;

import org.springframework.data.repository.CrudRepository;

import com.hcl.PetsAppController.model.Pet;

public interface PetRepository extends CrudRepository<Pet, Integer>{

}
