package com.hcl.PetsAppController.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.hcl.PetsAppController.model.Pet;
import com.hcl.PetsAppController.model.User;

public interface UserRepository extends CrudRepository<User, Integer>{
	//SELECT o FROM PurchaseOrder o JOIN Item i ON o.id = i.order.id WHERE i.id = :itemId", PurchaseOrder.class
  //@Query("select p from Pet p JOIN User u on  p.owner=u.userid where u.userid=:owner")
  //select p from com.hcl.PetsAppController.model.Pet p where p.owner_id:=userid
	//public List<Pet> getAllMyPets(@Param(value = "userid") int userid);
}
