package com.hcl.PetsAppController.model;



import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "pets")
public class Pet implements Serializable {
     
	private static final long serialVersionUID = 1L;
	@Id
      @GeneratedValue(strategy=GenerationType.AUTO)
      @Column(name = "ID")
      private int id;
      private String name;
      private int age;
      private String place;

      
      @ManyToOne(cascade=CascadeType.ALL)
      @JoinColumn(name = "userId", nullable = false)
          private User owner;

      public Pet() {
            super();
      }

      public Pet(int id, String name, int age, String place, User owner) {
            super();
            this.id = id;
            this.name = name;
            this.age = age;
            this.place = place;
            this.owner = owner;
            
      }
      
      

      public Pet(String name, int age, String place, User owner) {
            super();
            this.name = name;
            this.age = age;
            this.place = place;
            this.owner = owner;
            
      }

      public int getId() {
            return id;
      }

      public void setId(int id) {
            this.id = id;
      }

      public String getName() {
            return name;
      }

      public void setName(String name) {
            this.name = name;
      }

      public int getAge() {
            return age;
      }

      public void setAge(int age) {
            this.age = age;
      }

      public String getPlace() {
            return place;
      }

      public void setPlace(String place) {
            this.place = place;
      }

      public User getOwner() {
            return owner;
      }

      public void setOwner(User owner) {
            this.owner = owner;
      }

      

      
}
