package com.hcl.PetsAppController.model;


import java.util.Set;

import java.io.Serializable;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "user")
public class User implements Serializable{
     
	public User(String username, String userpassword, String confirmpassword) {
		super();
		this.username = username;
		this.userpassword = userpassword;
		this.confirmpassword = confirmpassword;
	}
	private static final long serialVersionUID = 1L;
	@Id
      @GeneratedValue(strategy=GenerationType.AUTO)
      @Column(name = "ID")
        private int userid;
      @Column(unique = true, nullable = false)
      private String username;
      private String userpassword;
      private String confirmpassword;
      @OneToMany(cascade=CascadeType.ALL,
              mappedBy = "owner")

      private Set<Pet> pets;
      public User() {
            super();
   
            
}
	public User(String username, String userpassword, String confirmpassword, Set<Pet> pets) {
		super();
		this.username = username;
		this.userpassword = userpassword;
		this.confirmpassword = confirmpassword;
		this.pets = pets;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUserpassword() {
		return userpassword;
	}
	public void setUserpassword(String userpassword) {
		this.userpassword = userpassword;
	}
	public String getConfirmpassword() {
		return confirmpassword;
	}
	public void setConfirmpassword(String confirmpassword) {
		this.confirmpassword = confirmpassword;
	}
	public Set<Pet> getPets() {
		return pets;
	}
	public void setPets(Set<Pet> pets) {
		this.pets = pets;
	}
}