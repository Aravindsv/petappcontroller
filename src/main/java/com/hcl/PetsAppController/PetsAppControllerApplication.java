package com.hcl.PetsAppController;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PetsAppControllerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PetsAppControllerApplication.class, args);
	}

}
