package com.hcl.PetsAppController.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import com.hcl.PetsAppController.model.User;
import com.hcl.PetsAppController.service.RecordNotFoundException;
import com.hcl.PetsAppController.service.UserService;



@CrossOrigin(origins="http://localhost:4200")
@RestController
public class UserController {

	
	@Autowired(required=true)
	public UserService userService;
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	@GetMapping("/listUsers")
	public ResponseEntity<List<User>> listUsers() {
        List<User> list = userService.listUsers();
 
        return new ResponseEntity<List<User>>(list, new HttpHeaders(), HttpStatus.OK);
    }
	 @GetMapping("/getUserById/{id}")
	    public ResponseEntity<User> getUserById(@PathVariable("id") int id) 
	                                                    throws RecordNotFoundException {
		 User user = userService.getUserById(id);
	 
	        return new ResponseEntity<User>(user, new HttpHeaders(), HttpStatus.OK);
	    }
	 @PostMapping("/create")
	    public ResponseEntity<User> addUser(@RequestBody User user)
	                                                    throws RecordNotFoundException {
	        User user1 = userService.addUser(user);
	        return new ResponseEntity<User>(user1, new HttpHeaders(), HttpStatus.OK);
	    }
	 @DeleteMapping("/delete/{id}")
	    public HttpStatus deleteEmployeeById(@PathVariable("id") int id) 
	                                                    throws RecordNotFoundException {
	        userService.removeUser(id);
	        return HttpStatus.FORBIDDEN;
	    }
	 /*@GetMapping(value="/getallpets/{id}")
	 public List<Pet> myPets(@PathVariable int id){
		 return userService.getAllMyPets(id);
	 }*/
	                                                   
	                                                
}


