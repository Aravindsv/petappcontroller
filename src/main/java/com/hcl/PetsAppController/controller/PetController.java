package com.hcl.PetsAppController.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.PetsAppController.model.Pet;

import com.hcl.PetsAppController.service.PetService;
import com.hcl.PetsAppController.service.RecordNotFoundException;

@RestController
public class PetController {
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(PetController.class);
	@Autowired
	private PetService petService;
	@GetMapping("/fetchAll")
	public ResponseEntity<List<Pet>> fetchAll() {
        List<Pet> list = petService.fetchAll();
 
        return new ResponseEntity<List<Pet>>(list, new HttpHeaders(), HttpStatus.OK);
    }
	@GetMapping("/getPetById/{id}")
    public ResponseEntity<Pet> getPetById(@PathVariable("id") int id) 
                                                    throws RecordNotFoundException {
	 Pet pet = petService.getPetById(id);
 
        return new ResponseEntity<Pet>(pet, new HttpHeaders(), HttpStatus.OK);
    }
 @PostMapping("/savePet")
    public ResponseEntity<Pet> savePet(@RequestBody Pet pet)
                                                    throws RecordNotFoundException {
        Pet pet1 = petService.savePet(pet);
        return new ResponseEntity<Pet>(pet1, new HttpHeaders(), HttpStatus.OK);
    }
	
	
	
}
